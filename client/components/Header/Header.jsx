import React from 'react'
import SearchIcon from '@material-ui/icons/Search'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import PersonIcon from '@material-ui/icons/Person'
import MenuIcon from '@material-ui/icons/Menu'

import { IconLink } from '~/components/IconLink/IconLink'
import { Search } from '~/components/Search/Search'

import styles from './Header.scss'

import logoImage from '~/assets/logo.png'

const Header = () => {
  return <header className={styles.header}>
    <div className={styles.navbar}>
      <div className={styles.left}><a href='https://www.platt.com/'><img src={logoImage} /></a></div>
      <nav className={styles.right}>
        <IconLink link='https://www.platt.com/'><SearchIcon/></IconLink>
        <IconLink link='https://www.platt.com/'><ShoppingCartIcon/></IconLink>
        <IconLink link='https://www.platt.com/'><PersonIcon/></IconLink>
        <IconLink link='https://www.platt.com/'><MenuIcon/></IconLink>
      </nav>
    </div>
    <Search/>
  </header>
}

export { Header }
