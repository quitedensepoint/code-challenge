import React from 'react'
import GoogleFontLoader from 'react-google-font-loader'

import { Header } from '~/components/Header/Header'
import { Navigation } from '~/components/Navigation/Navigation'
import { ProductMarkets } from '~/components/ProductMarkets/ProductMarkets'
import { DailyDeals } from '~/components/DailyDeals/DailyDeals'
import { RecentlyViewed } from '~/components/RecentlyViewed/RecentlyViewed'
import { AppAd } from '~/components/AppAd/AppAd'
import { Footer } from '~/components/Footer/Footer'
import { MobileActions } from '~/components/MobileActions/MobileActions'
import { PlattPlus } from '~/components/PlattPlus/PlattPlus'
import { NavCard } from '~/components/NavCard/NavCard'
import { WithContainer } from '~/components/WithContainer/WithContainer'

import superToolDayImage from '~/assets/super-tool-day.jpg'
import branchLocationsImage from '~/assets/ic_location_b.png'
import helpCenterImage from '~/assets/ic_helpcenter.png'
import plattUImage from '~/assets/ic_platt-u.png'

import styles from './App.scss'

const App = () => {
  return (
    <>
      <GoogleFontLoader
        fonts={[
          {
            font: 'PT Sans',
            weights: [400, 700]
          },
          {
            font: 'Darker Grotesque',
            weights: [900]
          }
        ]}
        subsets={['latin']}
      />
      <Header/>
      <Navigation/>
      <main>
        <MobileActions/>
        <ProductMarkets/>
        <WithContainer className={styles.allCards}>
          <WithContainer className={styles.dailyDeals}>
            <DailyDeals/>
          </WithContainer>
          <WithContainer className={styles.plattPlus}>
            <PlattPlus/>
          </WithContainer>
          <WithContainer className={styles.recentlyViewed}>
            <RecentlyViewed/>
          </WithContainer>
          <WithContainer className={styles.cardGrid}>
            <NavCard image={superToolDayImage} link='https://www.platt.com/' title='Super Tool Day &amp; Dynamic Data Day' centeredImage/>
            <NavCard image={branchLocationsImage} link='https://www.platt.com/' title='Branch Locations' centeredImage/>
            <NavCard image={helpCenterImage} link='https://www.platt.com/' title='Help Center' centeredImage/>
            <NavCard image={plattUImage} link='https://www.platt.com/' title='Platt University' centeredImage/>
          </WithContainer>
          <WithContainer className={styles.appAd}>
            <AppAd/>
          </WithContainer>
        </WithContainer>
      </main>
      <Footer/>
    </>
  )
}

export { App }
