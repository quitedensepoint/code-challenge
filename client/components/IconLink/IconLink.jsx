import React from 'react'
import PropTypes from 'prop-types'

import cx from 'classnames'

import styles from './IconLink.scss'

const IconLink = props => {
  const { link, children, className } = props
  return <a className={cx(className, styles.iconLink)} href={link}>
    {children}
  </a>
}

IconLink.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  link: PropTypes.string,
  className: PropTypes.string
}

export { IconLink }
