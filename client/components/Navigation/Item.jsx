import React from 'react'
import PropTypes from 'prop-types'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'

import styles from './Item.scss'

const Item = props => {
  const { link, title, children } = props
  return <a href={link} className={styles.link}>
    <div className={styles.left}>
      <span className={styles.icon}>{children}</span>
      <span className={styles.title}>{title}</span>
    </div>
    <div className={styles.right}>
      <ArrowForwardIcon/>
    </div>
  </a>
}

Item.propTypes = {
  link: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

export { Item }
