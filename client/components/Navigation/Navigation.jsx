import React from 'react'
import PlaylistCheckIcon from '@material-ui/icons/PlaylistAddCheck'
import ListIcon from '@material-ui/icons/List'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import PersonIcon from '@material-ui/icons/Person'
import PlaceIcon from '@material-ui/icons/Place'

import { Item } from './Item'
import { ChatActive } from '~/components/Icons/ChatActive/ChatActive'
import { MenuTag } from '~/components/Icons/MenuTag/MenuTag'

import styles from './Navigation.scss'

const Navigation = () => {
  return <nav className={styles.navigation}>
    <Item className={styles.fullWidth} link='google.com' title='Shop all categories'><MenuTag/></Item>
    <Item link='google.com' title='My Catalog'><PlaylistCheckIcon/></Item>
    <Item link='google.com' title='My List'><ListIcon/></Item>
    <Item link='google.com' title='Live Help'><ChatActive/></Item>
    <Item link='google.com' title='PIM'><CheckCircleIcon/></Item>
    <Item link='google.com' title='Account'><PersonIcon/></Item>
    <Item link='google.com' title='Branch Locations'><PlaceIcon/></Item>
  </nav>
}

export { Navigation }
