import React from 'react'
import PropTypes from 'prop-types'

import styles from './Capsule.scss'

const Capsule = props => {
  const { backgroundColor, children, link } = props
  return <a href={link} className={styles.capsule} style={{
    backgroundColor
  }}>
    {children}
  </a>
}

Capsule.propTypes = {
  backgroundColor: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  link: PropTypes.string
}

export { Capsule }
