import React from 'react'

import { Card } from '~/components/Card/Card'

import firstRecentlyViewed from '~/assets/11.jpg'
import secondRecentlyViewed from '~/assets/22.jpg'

import styles from './RecentlyViewed.scss'

const RecentlyViewed = () => {
  return (
    <Card className={styles.card}>
      <div className={styles.topBar}>
        <span>Recently Viewed</span>
        <div className={styles.viewAll}><a href='https://www.platt.com/'>View all</a></div>
      </div>
      <div className={styles.items}>
        <a href='https://platt.com/'><img src={firstRecentlyViewed}/></a>
        <a href='https://platt.com/'><img src={secondRecentlyViewed}/></a>
        <a href='https://platt.com/'><img src={secondRecentlyViewed}/></a>
      </div>
    </Card>
  )
}

export { RecentlyViewed }
