import React from 'react'

import { Card } from '~/components/Card/Card'
import { Capsule } from '~/components/Capsule/Capsule'
import { WithContainer } from '~/components/WithContainer/WithContainer'

import plattPlusImage from '~/assets/platt-plus.png'

import styles from './PlattPlus.scss'

const PlattPlus = () => {
  return (
    <Card>
      <WithContainer className={styles.plattPlus}>
        <img className={styles.mainImage} src={plattPlusImage}/>
        <div className={styles.textContent}>
          <h1 className={styles.main}>Get Started Today</h1>
          <h2 className={styles.secondary}>Become a <span className={styles.emphasis}>Platt Plus</span> Member.</h2>
          <Capsule link='https://platt.com/'>Learn More</Capsule>
        </div>
      </WithContainer>
    </Card>
  )
}

export { PlattPlus }
