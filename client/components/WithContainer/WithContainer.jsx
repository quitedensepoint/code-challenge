import React from 'react'
import PropTypes from 'prop-types'

const WithContainer = props => {
  const { children, className } = props
  return <div className={className}>{children}</div>
}

WithContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  className: PropTypes.string
}

export { WithContainer }
