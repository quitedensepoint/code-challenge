import React from 'react'

import { NavCard } from '~/components/NavCard/NavCard'

import dataCommImage from '~/assets/datacomm.jpg'
import hvacImage from '~/assets/hvac.jpg'
import industrialImage from '~/assets/industrial.jpg'
import lightingImage from '~/assets/lighting.jpg'
import energyImage from '~/assets/energy.jpg'
import utilityImage from '~/assets/utility.jpg'

import styles from './ProductMarkets.scss'

const ProductMarkets = () => {
  return <div className={styles.productMarkets}>
    <h1 className={styles.title}>Product Markets</h1>
    <div className={styles.cards}>
      <NavCard title='DataComm' image={dataCommImage} link='https://platt.com'/>
      <NavCard title='HVAC' image={hvacImage} link='https://platt.com'/>
      <NavCard title='Industrial' image={industrialImage} link='https://platt.com'/>
      <NavCard title='Lighting' image={lightingImage} link='https://platt.com'/>
      <NavCard title='Energy Efficiency' image={energyImage} link='https://platt.com'/>
      <NavCard title='Utility' image={utilityImage} link='https://platt.com'/>
    </div>
  </div>
}

export { ProductMarkets }
