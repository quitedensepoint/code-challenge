import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

import { Card } from '~/components/Card/Card'

import styles from './NavCard.scss'

const NavCard = props => {
  const { centeredImage, title, link, image } = props
  return <Card>
    <a className={cx(styles.cardLink, centeredImage ? styles.centeredImage : {})} href={link}>
      <div className={styles.navCard}>
        <div className={styles.image}>
          <img className={styles.image} src={image}></img>
        </div>
        <div className={styles.text}>{title}</div>
      </div>
    </a>
  </Card>
}

NavCard.propTypes = {
  centeredImage: PropTypes.bool,
  title: PropTypes.string,
  link: PropTypes.string,
  image: PropTypes.string
}

export { NavCard }
