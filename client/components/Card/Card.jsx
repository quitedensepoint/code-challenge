import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'

import styles from './Card.scss'

const Card = props => {
  const { children, className } = props
  return <div className={cx(styles.card, className)}>
    {children}
  </div>
}

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  className: PropTypes.string
}

export { Card }
