import React from 'react'

import { Card } from '~/components/Card/Card'

import appAdImage from '~/assets/app-get-banner.jpg'

import styles from './AppAd.scss'

const AppAd = props => {
  return (
    <Card>
      <a href='https://platt.com/' className={styles.link}>
        <img src={appAdImage} className={styles.fullImage}></img>
        <h2 className={styles.text}>Shop better, download the Platt App today</h2>
      </a>
    </Card>
  )
}

export { AppAd }
