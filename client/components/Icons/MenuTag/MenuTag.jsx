import React from 'react'
import MenuIcon from '@material-ui/icons/Menu'
import LocalOfferIcon from '@material-ui/icons/LocalOffer'

import styles from './MenuTag.scss'

const MenuTag = () => {
  return <span className={styles.wrapper}>
    <MenuIcon className={styles.menu}/>
    <LocalOfferIcon className={styles.tag}/>
  </span>
}

export { MenuTag }
