import React from 'react'
import ChatBubbleIcon from '@material-ui/icons/ChatBubble'
import AddIcon from '@material-ui/icons/Add'

import styles from './ChatActive.scss'

const ChatActive = () => {
  return <span className={styles.wrapper}>
    <ChatBubbleIcon className={styles.bubble}/>
    <AddIcon className={styles.add}/>
  </span>
}

export { ChatActive }
