import React, { useCallback, useRef, useState } from 'react'
import SearchIcon from '@material-ui/icons/Search'
import cx from 'classnames'
import 'regenerator-runtime/runtime'

import { IconLink } from '~/components/IconLink/IconLink'

import styles from './Search.scss'

const Search = () => {
  const searchInputRef = useRef(null)
  const [currentSuggestions, setCurrentSuggestions] = useState([])
  const clearSuggestions = useCallback(() => setCurrentSuggestions([]), [])
  const onSearchInput = useCallback(async e => {
    const value = e.target.value
    if (value.length === 0) {
      setCurrentSuggestions([])
      return
    }
    const localStorageValue = localStorage.getItem(value)
    if (localStorageValue) {
      setCurrentSuggestions(localStorageValue.split(','))
    } else {
      const response = await fetch('/autofill?word=' + value)
      const suggestions = await response.json()
      localStorage.setItem(value, suggestions)
      if (suggestions.length) {
        setCurrentSuggestions(suggestions)
      }
    }
  }, [])
  const applySuggestion = useCallback(e => {
    const suggestionValue = e.target.textContent
    searchInputRef.current.value = suggestionValue
    setCurrentSuggestions([])
  }, [])
  return <div className={styles.search}>
    <form className={styles.form}>
      <input ref={searchInputRef} onBlur={clearSuggestions} onFocus={onSearchInput} className={cx(styles.searchInput, currentSuggestions.length ? styles.suggestionsOpen : {})} type='text' placeholder='What are you looking for?' onChange={onSearchInput}/>
      <IconLink className={styles.searchIcon} link='https://www.platt.com/'><SearchIcon/></IconLink>
    </form>
    <div className={styles.suggestions}>
      {currentSuggestions.slice(0, 5).map(suggestion => {
        return <div key={suggestion} className={styles.suggestion} onClick={applySuggestion}>{suggestion}</div>
      })}
    </div>
  </div>
}

export { Search }
