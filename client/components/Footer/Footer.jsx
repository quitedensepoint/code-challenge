import React from 'react'
import cx from 'classnames'
import ChatIcon from '@material-ui/icons/Chat'

import { Capsule } from '~/components/Capsule/Capsule'

import styles from './Footer.scss'

const Footer = () => {
  return <footer className={styles.footer}>
    <div className={styles.center}>
      <div>Support Center</div>
      <div>
        <address>800-25-Platt</address>
        <address className={`${styles.smaller} ${styles.deEmphasized}`}>800-257-5288</address>
      </div>
      <div>
        <div className={styles.deEmphasized}>4 a.m. - 12 a.m. (pst)</div>
        <div className={styles.deEmphasized}>7 Days a week</div>
      </div>
      <Capsule link='https://platt.com/'>
        <div className={styles.liveHelp}>
          <ChatIcon/>
                Live Help
        </div>
      </Capsule>
    </div>
    <a className={cx(styles.left, styles.bottom)} href='https://www.platt.com/'>Contact Us</a>
    <a className={cx(styles.bottom, styles.middle)} href='https://www.platt.com/'>{'What\'s New'}</a>
    <a className={cx(styles.bottom, styles.right)} href='https://www.platt.com/'>{'FAQ\'s'}</a>
  </footer>
}

export { Footer }
