import React from 'react'
import LineStyleIcon from '@material-ui/icons/LineStyle'
import SearchIcon from '@material-ui/icons/Search'

import { Button } from './Button'

import scannerImage from '~/assets/green/ic_scanner.png'

import styles from './MobileActions.scss'

const MobileActions = () => {
  return <div className={styles.mobileActions}>
    <Button link='https://www.platt.com/' label='Categories'><LineStyleIcon/></Button>
    <Button link='https://www.platt.com/' label='Search'><SearchIcon/></Button>
    <Button link='https://www.platt.com/' label='Scan'><img src={scannerImage}/></Button>
  </div>
}

export { MobileActions }
