import React from 'react'
import PropTypes from 'prop-types'

import styles from './Button.scss'

const Button = props => {
  const { children, link, label } = props
  return (
    <div className={styles.buttonRoot}>
      <a href={link} className={styles.button}>
        {children}
      </a>
      <a href={link} className={styles.label}>{label}</a>
    </div>
  )
}

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  link: PropTypes.string,
  label: PropTypes.string
}

export { Button }
