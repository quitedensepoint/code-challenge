import React from 'react'

import { Card } from '~/components/Card/Card'

import dailyDealsImage from '~/assets/deal.jpg'

import styles from './DailyDeals.scss'

const DailyDeals = () => {
  return (
    <Card>
      <img src={dailyDealsImage} className={styles.dealImg}/>
    </Card>
  )
}

export { DailyDeals }
