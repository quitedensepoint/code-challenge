const express = require('express');
const http = require('http');
const path = require('path');
const staticPath = path.resolve('dist');

const app = express();
const port = 8080;

app.use(express.static(staticPath));

app.get('/autofill', (req, mainRes) => {
    const suggestedWord = req.query.word;
    mainRes.set('Access-Control-Allow-Origin', '*');
    http.get('http://api.datamuse.com/sug?s=' + suggestedWord, (res) => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];

        let error;
        if (statusCode !== 200) {
            error = new Error('Request Failed.\n' +
                            `Status Code: ${statusCode}`);
        } else if (!/^application\/json/.test(contentType)) {
            error = new Error('Invalid content-type.\n' +
                            `Expected application/json but received ${contentType}`);
        }
        if (error) {
            console.error(error.message);
            // Consume response data to free up memory
            res.resume();
            return;
        }

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
            try {
                const parsedData = JSON.parse(rawData);
                mainRes.send(JSON.stringify(parsedData.map(data => data.word)));
            } catch (e) {
                console.error(e.message);
            }
        });
    }).on('error', (e) => {
        console.error(`Got error: ${e.message}`);
    });
})

app.listen(port, () => console.log(`Listening on port ${port}`));