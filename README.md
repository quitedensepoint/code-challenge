# Coding Challenge

To launch the server, run
`yarn prod`

The server will be available at http://localhost:8080

For development, run `yarn dev`.
The dev server will be available at http://localhost:1234
